# WebMan
A web interface for the Arch Linux package manager (pacman)

![Screenshot](http://i.imgur.com/xABy47z.png)

This app based on Flask (Python) allows you to manage your Arch Linux packages in a graphical way. You can browse packages just like on the official Arch Linux website, with an upgraded UI and an install/unintall button next to them.

*At the moment this app in in heavy development so don't expact things to work properly yet.*
